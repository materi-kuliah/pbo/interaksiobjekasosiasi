/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterksiObjekKomposisi;

/**
 *
 * @author mymac
 */
public class Komputer {
    String merk;
    String tipe;
    
    Mouse mouse;
    Keyboard keyboard;
    Cpu cpu;
    
    Komputer(String merk, String tipe){
        this.merk = merk;
        this.tipe = tipe;
    }
    
    public void AddKomputer(Mouse mouse, Keyboard keyboard, Cpu cpu){
        this.mouse = mouse;
        this.keyboard = keyboard;
        this.cpu = cpu;
    }
    
    public void displayDataKomputer(){
        System.out.println("Merk Komputer : "+ this.merk);
        System.out.println("Tipe Komputer : "+ this.tipe);
        
        System.out.println("Modeul CPU : "+this.cpu);
        System.out.println("Jenis Keyboard : "+ this.keyboard);
        System.out.println("Merk Mouse : "+this.mouse);
    }
}
