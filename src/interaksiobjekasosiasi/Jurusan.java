/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interaksiobjekasosiasi;

/**
 *
 * @author mymac
 */
public class Jurusan {
    String kodeJurusan;
    String namaJurusan;
    
    Mahasiswa[] daftar = new Mahasiswa[10];
    int jmlMhs;
    
    Jurusan(String kodeJurusan, String namaJurusan){
        this.kodeJurusan = kodeJurusan;
        this.namaJurusan = namaJurusan;
    }
    
    public void addMahasiswa(Mahasiswa mahasiswa){
        this.daftar[jmlMhs] = mahasiswa;
        this.jmlMhs++;
    }
    
    public void displayMahasiswaJurusan(){
        System.out.println("Kode Jurusan : "+this.kodeJurusan);
        System.out.println("Nama Jurusan : "+this.namaJurusan);
        System.out.println("Daftar Mahasiswa");
        for(int i=0;i<jmlMhs;i++){
            System.out.println(daftar[i].getNim()+" "+daftar[i].getNama());
        }
    }
}
