/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interaksiobjekasosiasi;

/**
 *
 * @author mymac
 */
public class InteraksiObjekAsosiasi {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
    /**
     * Input Mahasiswa
     */
        Mahasiswa mahasiswa1 = new Mahasiswa("089234", "Rina");
        Mahasiswa mahasiswa2 = new Mahasiswa("567812", "Aliando");
        
    /**
     * Input Dosen
     */
     /*   Dosen dosen = new Dosen("Iwan", "Ds001");
        dosen.setNIMMhs("1234567");
        dosen.setNIMMhs("9870923");
        
    /**
     * Display Data
     */
      /*  System.out.println("Kode Dosen : "+dosen.getKdDosen());
        System.out.println("Nama Dosen : "+dosen.getNamaDosen());
        System.out.println("Jumlah Mahasiswa : "+dosen.getJmlMhs());
        System.out.println("Daftar Mahasiswa");
        
        for(int i=0;i<dosen.getJmlMhs();i++){
            System.out.println(dosen.getMhs(i));
        }
        */
      
      /**
       * Input Jurusan
       */
      Jurusan jurusan = new Jurusan("Fak.1290", "Informatika");
      jurusan.addMahasiswa(mahasiswa1);
      jurusan.addMahasiswa(mahasiswa2);
      
      /**
       * Display mahasiswa Jurusan
       */
      
      jurusan.displayMahasiswaJurusan();
      
    }
   
}
