/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interaksiobjekasosiasi;

/**
 *
 * @author mymac
 */
public class Dosen {
    String nama;
    String kdDosen;
    
    String[] nimMhs = new String[5];
    int jmlMhs;
    
    Dosen(String nama, String kdDosen){
        this.nama = nama;
        this.kdDosen = kdDosen;
    }
    
    public String getNamaDosen(){
        return nama;
    }
    
    public String getKdDosen(){
        return kdDosen;
    }
    
    public void setNIMMhs(String nimMHS){
        nimMhs[jmlMhs] = nimMHS;
        jmlMhs++;
    }
    
    public String getMhs(int i){
        return nimMhs[i];
    }
    
    public int getJmlMhs(){
        return jmlMhs;
    }
    
}
